import { LoginService } from './../services/login.service';
import { MediaService } from './../services/media.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.css']
})
export class MediaComponent implements OnInit {


  private fileid: any;
  private media: any = null;
  private user: any = null;
  private userid;
  private likes:any = [];

  constructor(private route: ActivatedRoute, private mediaService: MediaService, private router: Router) {
    this.media = "asd";
    this.user="asd";
  }

  ngOnInit() {
      this.route.params
     .subscribe( (params: any) => {
       this.fileid = params.fileId;
  });

  this.getMediaAndUser(this.fileid);

  }
 getMediaAndUser(fileid:number) {
      this.mediaService.getMediaById(fileid)
      .subscribe((res) =>  {
        this.media = res;

        this.mediaService.getUserByUserId(res.user_id)
        .subscribe(resp => this.user = resp);

        this.mediaService.getLikesByMediaId(res.file_id)
        .subscribe(respo => this.likes = respo);
      });
  }


}
